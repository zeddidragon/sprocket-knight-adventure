if (state != "idle"){
    if (hasChargeDisplay){
        hasChargeDisplay = false;
        with (chargeDisplay){
            instance_destroy();
        }
    }
    
    if (hasChargeSound){
        hasChargeSound = false;
        audio_stop_sound(chargeSound);
    }
    
    nextAiState = "idle";
}
