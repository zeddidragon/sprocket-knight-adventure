if (!canAttack){
    nextState = "none";
    return false;
}

sprite_index = shieldStanceSprite;
image_index = 0;
image_speed = 0;


if (shield < 0){
    shield = instance_create(x, y, wpnShield);
}
shield.master = id;
shield.state = 1;
shield.image_xscale = image_xscale;
isShielding = true;
shieldDirection = 180 * (image_xscale < 0);

sndPlay3d(sndShield, false);

buffApply(BUFF_MSI, "shield", 0.5);
