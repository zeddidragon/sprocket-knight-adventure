///generateWeapon(int index, objId weaponObject)
var _ret;
_ret = instance_create(x, y, argument1);

weapon[argument0] = _ret;
_ret.parent = id;
if (argument0 == 1){
  _ret.button = "sidearm";
} else {
    _ret.chargePattern = 1;
}

_ret.slotNumber = argument0;

return _ret;