///getHit(real damage, int stagger, int knockback, real direction)

var _weight, _knockback, _damage;

if (hp == 0){
    return false;
}

_weight = weight;
_damage = argument0;

if (isShielding){
    var _angleDif;
    _angleDif = abs((shieldDirection - argument3)) mod 360;
    if (_angleDif > 90 && _angleDif < 270){
        sndPlay3d(sndShieldTink, false);
        _weight += shieldWeight;
        _damage = 0;
    }
}

if (_damage > 0){
    damagePips(x, y, argument0);
    hp = clamp(hp - argument0, 0, maxHp);
    sndPlay3d(onHitSound, false);
    flashWhite();
}

getKnockback(max(argument2 - _weight, 0) * 0.01, argument3);