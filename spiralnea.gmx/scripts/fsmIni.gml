///fsmIni()
/*
Initializes data structures required by the finite state machine.
Requires dscIni() to be run first
*/
startStateMap = dscGenMap();
stateMap = dscGenMap();
nextState = "none";
state = "none";

aiStartStateMap = dscGenMap();
aiStateMap = dscGenMap();
nextAiState = "none";
aiState = "none";

fsmAddState("animate", dummyScript, fsmAnimationState);
fsmAddState("fsm-delay", dummyScript, fsmIdleState);
fsmAddState("none", dummyScript, dummyScript);
fsmAddState("spawn", dummyScript, dummyScript);
fsmAddAi("none", dummyScript, dummyScript);