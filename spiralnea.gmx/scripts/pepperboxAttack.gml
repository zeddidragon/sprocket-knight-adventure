///pepperboxAttack()
advanceFrame(id, 0, parent);
if (ammo > 0){
    parent.image_index = image_index;
}
    
if (isAtFrame(id, 6)){
    combo += 1;
    if (ammo > 0){

        shot = spawnShot(shotX, shotY, oPepperShot, -5 + 5 * (combo mod 3) - aimHeight * 22.5);
        shot.hasFire = consumeMist(parent, parent.hasMagic, 10);
      
        spawnAnimation(0, 0, muzzleSprite, 1, image_xscale, 0);
        part_particles_create(PartSys, x + casingX, y + casingY, PartPepperCasing, 1);
        ammo -= 1;
        sndPlay3d(sndGunShot, false);
    } else {
        sndPlay3d(sndGunClick, false);
    }
}

if (isAtFrame(id, image_number)){
    if (combo < 3){
        image_index = 3;
    } else if (willAttack){
        combo = 0;
        inputQueueConsumeWeapon();
        image_index = 3;
        pepperboxAim();
    } else {
        nextState = "holster";
    }
}