///sideCollision(objId object, sign side)

if (argument1 == 0 || !isSolid){
    return false;
} else {
    var _my;
    _my = id; 
    
    with (argument0) {
        if (
            abs(_my.x - x) <= width + _my.width &&
            abs(_my.y - y + (height - _my.height) * 0.5) <=
                (_my.height + height) * 0.5 &&
            isSolid
        ){
            if (xprevious > _my.xprevious && argument1 > 0){
                _my.x = x - width - _my.width - 1;
            } else if (xprevious < _my.xprevious && argument1 < 0){
                _my.x = x + width + _my.width + 1;
            }
        }
    }
}