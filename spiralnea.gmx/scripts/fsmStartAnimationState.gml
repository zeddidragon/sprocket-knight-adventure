///fsmStartAnimationState(sprIndex sprite, real startFrame, real endFrame, realAnimationSpeed, string nextState)
sprite_index = argument0;

if (argument1 == -1){
    image_index = image_number + argument3;
} else {
    image_index = argument1;
}
if (argument2 == -1){
    fsmEndFrame = image_number;
} else {
    fsmEndFrame = argument2;
}

fsmAnimationSpeed = argument3;
fsmQueueState = argument4;

image_speed = 0;

nextState = "animate";