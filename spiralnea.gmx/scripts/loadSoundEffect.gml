//argument0: filename
//argument1: 3d true/false

var ret;
ret = FMODSoundAdd(argument0, argument1, false);
FMODSoundSetGroup(ret, 2);
if (argument1){
    FMODSoundSet3dMinMaxDistance(ret, 0, 400);
}

return ret;
