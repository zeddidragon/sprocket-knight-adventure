///triglavCharge()
if (combo == 1){
  advanceFrame(parent, 2, parent);
} else {
  parent.image_index = parent.image_number - 1;
}


if (parent.image_index < 5){
  parent.xspeed = parent.image_xscale * parent.image_index * 0.45 * parent.totalBuff[BUFF_MSI];
} else {
  parent.xspeed = 0;
}

swordHitbox(hitboxes[2, 0], oEnemy);

if (isAtFrame(parent, 2)){ // Pivot of swing
  sndPlay3d(sndSlash2, false);
} else if (isAtFrame(parent, 5)){ // Triglav hitting the ground
  sndPlay3d(sndHeavySwordStrike, false);
  
  with (parent){
    part_particles_create(PartSys, x, y, PartIcicle, 8);
    spawnAnimation(0, 0, sprIceSplotion, 0.5, 1, 0);
  }
} else if (isAtFrame(parent, parent.image_number) && combo == 1){ // Animation done
    parent.image_index = parent.image_number - 1;
  combo += 1;
  comboTimer = 15;
  
} else if (combo == 0){
  sndPlay3d(sndFreezesplotion, false);
  oControl.y = view_yview + view_hview * 0.5 - 8;
  panToObject(parent); 
  
  ds_list_clear(hitList);
  swordHitbox(hitboxes[2, 1], oEnemy);
  
  with (parent){
    part_particles_create(PartSys, x + 48 * image_xscale, y, PartIcicle, 8);
    spawnAnimation(48 * image_xscale, 0, sprIceSplotion, 0.5, 1, 0);
  }
  
  combo = 3;
  comboTimer = 30;
  
  nextState = "idle";
}