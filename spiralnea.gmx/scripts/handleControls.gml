///handleControls()

var key;
for (j = 0; j < PlayerCount; j += 1){
    var controller = ControllerMap[j];
    var gamepad = GamepadMap[j];
    var held = HeldMap[j];
    var pressed = PressedMap[j];
    var released = ReleasedMap[j];

    key = ds_map_find_first(controller);
    for (i = 0; i < ds_map_size(controller); i += 1){
        pressed[?key] = false;
        released[?key] = false;
        if (keyboard_check(controller[?key]) || gamepad_button_check(j, gamepad[?key])){
          if (!held[?key]){
              held[?key] = true;
              pressed[?key] = true;
          }
        } else if (held[?key]){
            held[?key] = false;
            released[?key] = true;
        }
        key = ds_map_find_next(controller, key);
    }
}
