///playerMoveV(bool hasFriction)

if (isButtonHeld(playerNumber, "down"))
  yspeed += totalAcceleration;
else if (isButtonHeld(playerNumber, "down"))
  yspeed -= totalAcceleration;

if (yspeed < -totalMaxSpeed)
  yspeed = -totalMaxSpeed;
else if (yspeed > totalMaxSpeed)
  yspeed = totalMaxSpeed;

if (y - height < 0)
  y = height;
else if (y + height > room_height)
  y = room_height-height;
  
if (argument0 && !(isButtonHeld(playerNumber, "up") || isButtonHeld(playerNumber, "down")))
  haveVFriction();