///pepperboxStartReload()
isVisible = true;
resetToIdle();
parent.canAttack = false;
image_index = 0;
if (nextState == "holster"){
    sprite_index = sprPepperHolster;
} else {
    sndPlay3d(sndGunReload, false);
    canAttack = false;
    sprite_index = sprPepperReload;
    ammo = 12;
    part_particles_create(PartSys, x + 21 * image_xscale, y - 16, PartPepperBarrel, 1);
}
