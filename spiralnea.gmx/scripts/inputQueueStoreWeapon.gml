///inputQueueStoreWeapon()
//Feed weapon with queued command.
//Must be called from parent of weapons.

inputQueueStore( parent,
    //Command type, number
    "weapon", slotNumber,
    //Side modifier
    (isButtonHeld(parent.playerNumber, "right") -
    isButtonHeld(parent.playerNumber, "left")
    ) * !isButtonHeld(parent.playerNumber, "shield"),
    //Height modifier
    isButtonHeld(parent.playerNumber, "down") -
    isButtonHeld(parent.playerNumber, "up")
);
