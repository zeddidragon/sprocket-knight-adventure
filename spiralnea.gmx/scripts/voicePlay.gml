///argument0 is name of sound clip to play (string)

var ret;
ret = FMODSoundPlay(
    ds_map_find_value(global.voiceClips, argument0),
    false
);
return ret;
