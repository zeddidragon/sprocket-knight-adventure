///weaponQueueClear()
//Must be called from weapon.
ds_queue_clear(weaponQueue);
weaponQueueCount = 0;

var _i;
for (_i = 0; _i < 2; _i++){
    if (weapon[_i].willAttack){
        weapon[_i].willAttack = false;
        weaponQueueReady = true;
    }
}
