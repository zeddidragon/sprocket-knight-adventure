///spawnAnimation(real xOffset, realYOffset, sprId sprite, real speed, int image_xscale, real angle)

with (instance_create(x + argument0 * argument4, y + argument1, oAnimationEffect)){
    image_xscale = argument4;
    sprite_index = argument2;
    image_speed = argument3;
    image_angle += argument5;
    return id;
}