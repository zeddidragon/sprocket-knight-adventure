///statusFind(string status)
/*
    returns attached status object if entity has a status applied.
    Otherwise returns -1
*/
if (ds_map_exists(statusObjectMap, argument0)){
    return statusObjectMap[?argument0];
} else {
    return noone;
}
