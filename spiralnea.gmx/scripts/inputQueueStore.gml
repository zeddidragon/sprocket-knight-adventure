///inputQueueStore(objId queueObject, string command type, args... stuff)

with (argument[0]){
    inputQueueClear();
    inputQueueCount++;
}

var _i;
for (_i = 1; _i < argument_count; _i++){
    ds_queue_enqueue(argument[0].inputQueue, argument[_i]);
}
