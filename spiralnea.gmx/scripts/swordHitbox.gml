///swordHitbox(objId hitboxObject, objId enemy)
//Handles collision checking for sword attacks

//First, confirm we're at the animaton frames where the sword should strike
if (parent.image_index > argument0.startFrame && parent.image_index < argument0.endFrame){
    generalHitbox(argument0, argument1, parent);
    environmentHitbox(argument0);
}