///calculateTotalBuff(const buffType)

totalBuff[argument0] = 1;

var _key;
_key = ds_map_find_first(buffMap[argument0]);

repeat (ds_map_size(buffMap[argument0])){
    totalBuff[argument0] *= ds_map_find_value(buffMap[argument0], _key);
    _key = ds_map_find_next(buffMap[argument0], _key);
}

if (argument0 == BUFF_MSI){
    totalAcceleration = RUN_ACC * totalBuff[argument0];
    totalMaxSpeed = RUN_SPEED * totalBuff[argument0];
}
