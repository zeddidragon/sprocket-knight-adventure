///spawnerSpawn(string sceneMarker, objId object)
marker = sceneMarker(argument0);
spawn = instance_create(marker.x, marker.y, argument1);
with (spawn){
    aiFaceTowards(oPlayer)
    nextState = "spawn";
}