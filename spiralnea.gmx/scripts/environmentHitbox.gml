///environmentHitbox(objId hitboxObject)

var _left, _top, _right, _bottom, _my;
_my = id;

_left   = parent.x   + argument0.left    * parent.image_xscale
_right  = parent.x   + argument0.right   * parent.image_xscale
_top    = parent.y   + argument0.top;
_bottom = parent.y   + argument0.bottom;

with (oEnvironmentalObject){
    if (collision_rectangle(_left, _top, _right, _bottom, id, false, false)){
        if (ds_list_find_index(_my.hitList, id) < 0){ //Confirm we haven't struck this enemy already during this attack
            script_execute(getHitScript);            
            ds_list_add(_my.hitList, id);   //Mark object as having been struck during this attack
        }
    }
}
