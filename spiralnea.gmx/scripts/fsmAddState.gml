///fsmAddState(string state, script startScript, script runningScript);
//argument0: state variable
//argument1: start state script
//argument2: running state script

startStateMap[?argument0] = argument1;
stateMap[?argument0] = argument2;
