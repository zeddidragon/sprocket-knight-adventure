chargeDisplay.x = x;
chargeDisplay.y = y;


if (!isButtonHeld(parent.playerNumber, button)){
    if (parent.canAttack){  
        charged = true;
        inputQueueStoreWeapon();
    }
    fsmAiStartIdleState(1, "idle");
}

if (state != "idle"){
    nextAiState = "idle";
}
