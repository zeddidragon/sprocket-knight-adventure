///damageNumber(x, y, damage)
var num;

if (argument2 < 0){
    num = instance_create(argument0 + random_range(-12, 12), argument1 + random_range(-12, 12), oHealNumber);
    num.number = -argument2;
} else {
    num = instance_create(argument0 + random_range(-12, 12), argument1 + random_range(-12, 12), oDamageNumber);
    num.number = argument2;
}
