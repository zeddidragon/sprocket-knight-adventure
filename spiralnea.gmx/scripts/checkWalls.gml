///checkWalls(signum speed)
//returns nonzero if collision. Less than zero means the collison was on the left side
//argument0: horizontal movement speed of the thing checking. Collisions to the right will be prioritized when moving right.

var highest, groundY, startx, endx, starty, endy, collision;

highest = room_height;
groundY = highest;
collision = 0;

startx = max(0, (x-width) div TILESIZE);
endx = min(MapWidth-1, floor(x+width) div TILESIZE);
starty = max(0, (y-8) div TILESIZE);
endy = min(MapHeight-1, (y-4) div TILESIZE);


for (j = starty; j <= endy; j += 1){
  for (i = startx; i <= endx; i += 1){
    if (Map[i,j] == BLOCK){
      if (argument0 < 0){
        return (i * TILESIZE) - x;
      } else {
        collision = ((i + 1) * TILESIZE) - x;
      }
    }
  }
}

return collision;
