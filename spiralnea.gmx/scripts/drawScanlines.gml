///drawScanlines(int width, int height, real scaling)
var width, height, newWidth, newHeight,
scaling, halfScaling,i, j;

width = argument0;
height = argument1;
scaling = argument2;
halfScaling = (scaling div 2);

newWidth = width*scaling;
newHeight = height*scaling;

draw_set_color(c_black);

/*
for (i = 0; i < newWidth; i+=1){
    draw_set_alpha((10^((i mod scaling)/(scaling-1)))/10);
    draw_line(i, 0, i, newHeight);
}
*/
draw_set_alpha(0.5);
if (scaling == 2){
    for (i = 0; i <= newHeight; i+=2){
        draw_line(0, i, newWidth, i);
    }
    draw_set_alpha(0.2);
    for (i = 0; i <= newWidth; i+=2){
        draw_line(i, 0, i, newHeight);
    }
} else if (scaling > 2){   
    for (i = 0; i <= newHeight; i+=scaling){
        draw_rectangle(0, i-halfScaling, newWidth, i, false);
    }
    draw_set_alpha(0.2);
    for (i = 0; i <= newWidth; i+=scaling){
        draw_rectangle(i-halfScaling, 0, i, newHeight, false);
    }
}

draw_set_color(c_white);
draw_set_alpha(1);