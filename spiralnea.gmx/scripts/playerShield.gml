playerMoveH(true, false);

if (xspeed != 0){
    if (isAtFrame(id, 2)){
        sndPlay3d(stepSound, false);
    }
    image_speed = xspeed * image_xscale; //Scroll frame
} else {
    image_index = 0;
    image_speed = 0;
}

if (!isButtonHeld(playerNumber, "shield")){
    playerUnshield();
    nextState = "idle";
}

if (!touchGround){
    playerUnshield();
    nextState = "fall";
}
