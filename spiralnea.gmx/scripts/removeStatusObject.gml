///removeStatusObject(objId statusObject)
var _obj;

for (i = 0; i < ds_list_size(statusObjectList); i++){
    _obj = ds_list_find_value(statusObjectList, i);
    
    if (_obj.object_index == argument0){
        with (_obj){
            instance_destroy();
        }
        ds_list_delete(statusObjectList, i);
        break;
   }
}
