///spawnChild()

if (instance_exists(oPlayer)){
  with (instance_create(x,y,child))
  image_xscale = sign(x-oPlayer.x);
}
instance_destroy();