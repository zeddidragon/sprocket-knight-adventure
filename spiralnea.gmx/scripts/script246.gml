aimHeight =
    isButtonHeld(parent.playerNumber, "down") -
    isButtonHeld(parent.playerNumber, "up");
      
parent.sprite_index = 
    (aimHeight < 0) * sprPepperUp +
    (aimHeight == 0) * sprPepperFront +
    (aimHeight > 0) * sprPepperDown;
    
muzzleSprite = 
    (aimHeight < 0) * sprPepperMuzzleUp +
    (aimHeight == 0) * sprPepperMuzzle +
    (aimHeight > 0) * sprPepperMuzzleDown;
    
shotX = 32 + aimHeight * 2;
shotY = -6 + aimHeight * 16;

casingX = (16 + aimHeight * 4) * image_xscale;
casingY = - 12 + aimHeight * 4;
