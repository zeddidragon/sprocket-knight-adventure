///checkCollision(objId object, real yOffset) returns instance
//argument0 is object to check for
//argument1 is how much to displace y2 from the objects y-value
//returns instance that is collided with

return collision_rectangle(x-width, y-height, x+width, y+argument1, argument0, false, true);