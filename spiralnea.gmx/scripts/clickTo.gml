///clickTo(string direction))
//argument 0: direction to click. Can be LEFT, RIGHT, UP or DOWN
//not that clicking RIGHT will attach the object's right side to the wall's

switch (argument0){
  case "left":
    x = ((x - width) & $FFFFFFF0) + TILESIZE + width;
    xspeed = 0;
    break;
  case "right":
    x = ((x + width) & $FFFFFFF0) - width - 1;
    xspeed = 0;
    break;
  case "up":
    y = ((y - height) & $FFFFFFF0) + TILESIZE + height;
    yspeed = 0;
    break;
  case "down":
    y = ((y + height) & $FFFFFFF0) - 1;;
    yspeed = 0;
    break;
}