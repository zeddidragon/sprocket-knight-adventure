///alignNode(objId object, real xOffset, realyOffset)

//argument0: object to align
//argument1: xoffset
//argument2: yoffset

argument0.xspeed = 0;
argument0.yspeed = 0;
argument0.image_index = image_index;
argument0.x = x+argument1*image_xscale;
argument0.y = y+argument2;
argument0.image_xscale = image_xscale;
