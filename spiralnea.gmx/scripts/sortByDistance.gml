///sortByDistance(objId object)
//returns a ds_list which you needs to be destroyed externally
var _ret, _grid, _i, _my;

_ret = ds_list_create();
_grid = ds_grid_create(2, instance_number(argument0));
_i = 0;
_my = id;

with (argument0){
    ds_grid_add(_grid, 0, _i, id);
    ds_grid_add(_grid, 1, _i, point_distance(_my.x, _my.y, x, y));
    _i++;
}

ds_grid_sort(_grid, 1, true);

for (_i = 0; _i < 0; _i++){
    ds_list_add(_ret, ds_grid_get(_grid, 0, _i));
}

ds_grid_destroy(_grid);

return _ret;
