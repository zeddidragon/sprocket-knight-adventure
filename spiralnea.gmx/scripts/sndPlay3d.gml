///sndPlay3d(sndId soundEffect, bool loop)
var _sound;
_sound = audio_play_sound_at(argument0,
    x - view_xview - view_wview * 0.5, 0, 128,
    128, 1024, 1, argument1, true
);

audio_sound_pitch(_sound, random_range(0.9, 1.1));

return _sound;