xspeed += totalAcceleration * sprintModifier * image_xscale;
if (abs(xspeed) > totalMaxSpeed * sprintModifier){
    xspeed = totalMaxSpeed * sprintModifier * image_xscale;
}

if (isAtFrame(id, 2) || isAtFrame(id, 4)){
    sndPlay3d(stepSound, false);
}


if (
    wasButtonPressed(playerNumber, "sprint") ||
    (xspeed < 0 && wasButtonPressed(playerNumber, "right")) ||
    (xspeed > 0 && wasButtonPressed(playerNumber, "left"))
){
  nextState = "sprint stop";
}
image_speed = 0.1 + 0.1 * abs(xspeed); //Scroll frame

playerGroundAction();
