///pepperboxStartAttack()
with (parent){
    if (state == "shield"){
        nextState = "idle";
        with (shield){
            instance_destroy();
        }
    }
    triglav.isVisible = true;
}


isVisible = false;
if (ammo <= 0){
    nextState = "reload";
    pepperboxStartReload();
} else {
    setToAttack();
    combo = 0;
    pepperboxAim();
}
