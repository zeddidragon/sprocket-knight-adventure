///applyStatus(string status, real strength)
var _total;

_total = clamp(
    argument1 - ds_map_find_value(statusResistanceMap, argument0),
    -6, 8
);

if (_total > -6){
    switch (argument0){
        case "stun":
            break;
        case "freeze":
            addStatusObject(argument0, statusFreeze, _total);
            break;
        case "fire":
            addStatusObject(argument0, statusFire, _total);
            break;
        case "shock":
            break;
        case "poison":
            break;
    }
}
