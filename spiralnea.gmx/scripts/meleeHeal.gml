///meleeHeal(objId hitboxObject, objId enemy)
if (argument1.hp == argument1.maxHp || argument1.hp == 0){
    return false;
}

sndPlay3d(sndHeal, false);

var _poison, _heal;
_poison = statusFind("poison");
_heal = argument0.damage;

if (_poison >= 0) with (_poison){
    statusInflictDamage(argument1, _heal);
} else {
    with (argument1){
        hp = clamp(hp + _heal, 0, maxHp);
        damageNumber(x, y, -_heal);
    }
}
