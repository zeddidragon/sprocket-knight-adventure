///addStatusObject(string name, statusObj object, real strength)

///Erase existing status
var _obj;

removeStatus(argument0);

_obj = instance_create(x, y, argument1);
_obj.target = id;
_obj.strength = argument2;
with (_obj){
    script_execute(startScript);
    sndPlay3d(inflictSound, false);
}

ds_map_add(statusObjectMap, argument0, _obj);
