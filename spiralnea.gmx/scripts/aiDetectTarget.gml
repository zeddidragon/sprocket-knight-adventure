///aiDetectTarget(objId target)
return instance_exists(argument0) &&
    collision_rectangle(
        x + aiDetectRange * image_xscale,   y - aiVerticalDetect,
        x,                                  y + aiVerticalDetect,
        argument0, false, true
);