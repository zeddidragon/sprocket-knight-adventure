///feetCollision(objId object)

var _my, _dir;
_my = id; 

if (!isSolid){
    return false;
}

with (argument0) {
    if ( x + width  > _my.x - _my.width &&
         x - width  < _my.x + _my.width &&
         y          > _my.y - _my.height &&
         y - height < _my.y && isSolid
    ){
        _my.y = y - height - 1;
        _dir = 1 - 2 * (_my.x < x && !isBlockAt(x - width - TILESIZE, y - 1) || isBlockAt(x + width + TILESIZE, y - 1))
        _my.xspeed = RUN_SPEED * _dir;
        getKnockback(1, 270 - 45 * _dir);
    }
}
