///statusRemove(string status)
if (ds_map_exists(statusObjectMap, argument0)){
    with (statusObjectMap[?argument0]){
        instance_destroy();
    }
    ds_map_delete(statusObjectMap, argument0);
}
