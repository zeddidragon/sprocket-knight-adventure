///fsmStartAnimationState()

var _nextFrame;
_nextFrame = image_index + fsmAnimationSpeed;

if ((fsmAnimationSpeed < 0 && _nextFrame <= fsmEndFrame) ||
    (fsmAnimationSpeed > 0 && _nextFrame >= fsmEndFrame) ){
    image_index = fsmEndFrame;
    
    if (fsmEndFrame == image_number){
        image_index = image_number - 1;
    }
    
    nextState = fsmQueueState;
    
} else  {
    image_index = _nextFrame;
}