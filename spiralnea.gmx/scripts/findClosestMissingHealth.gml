///findClosestMissingHealth(objId object)
var _list _i, _size, _value;;
_list = sortByDistance(argument0);
_size = ds_list_size(_list);

for (_i = 0; _i < _size; i++){
    _value = ds_list_find_value(_list, _i);
    if (_value != id && _value.hp < _value.maxHp){
        return _value;
    }
}

return -1;
