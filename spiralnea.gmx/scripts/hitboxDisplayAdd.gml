///hitboxDisplayAdd(left, top, right, bottom)
with (oHitboxDisplay){
    left[boxCount] = argument0;
    top[boxCount] = argument1;
    right[boxCount] = argument2;
    bottom[boxCount] = argument3;
    
    boxCount += 1;
}