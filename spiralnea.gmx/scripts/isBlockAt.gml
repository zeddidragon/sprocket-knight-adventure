///isBlockAt(real x, real y)

if (argument0 >= 0 && argument0 < room_width &&
    argument1 >= 0 && argument1 < room_height){
    return Map[argument0 div TILESIZE, argument1 div TILESIZE] == BLOCK;
} else {
    return true;
}
