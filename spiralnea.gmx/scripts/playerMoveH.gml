///playerMoveH(bool hasFriction, bool canTurn)

/*
    If you are looking at this script, it's possible because
    you're getting an error about it being called from a non-player character.
    
    This is the script with index 0.
    
    The state machine is probably receiving a state you haven't fed it. Check for spelling.
    
    You're welcome.
    
    --Tony from the past.
*/

var _canTurn;
_canTurn = argument1 && !isButtonHeld(playerNumber, "shield");

if (isButtonHeld(playerNumber, "right")){
  xspeed += totalAcceleration;
  if (_canTurn){
    image_xscale = 1;
  }
} else if (isButtonHeld(playerNumber, "left")){
  xspeed -= totalAcceleration;
  if (_canTurn){
    image_xscale = -1;
  }
}

if (xspeed < -totalMaxSpeed)
  xspeed = -totalMaxSpeed;
else if (xspeed > totalMaxSpeed)
  xspeed = totalMaxSpeed;

  
if (argument0 && !(isButtonHeld(playerNumber, "left") || isButtonHeld(playerNumber, "right")))
  haveFriction();