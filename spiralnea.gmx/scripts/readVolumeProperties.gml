//argument0: ds_map to iterate through
//argument1: section of ini-file to search

var __sound, _size, i;
_size = ds_map_size(argument0);
_sound = ds_map_find_first(argument0);

for (i = 0; i < _size; i+=1){
    FMODSoundSetMaxVolume(ds_map_find_value(argument0, _sound), ini_read_real(argument1, _sound, 1));
    _sound = ds_map_find_next(argument0, _sound);
}
