docIni();

//Hitboxes for combo attack 1, up, front, down
hitboxes[0,0] = docObjectCreate();
hitboxes[0,1] = docObjectCreate();
hitboxes[0,2] = docObjectCreate();

//Hitboxes for combo attack 2, up, front, down
hitboxes[1,0] = docObjectCreate();
hitboxes[1,1] = docObjectCreate();
hitboxes[1,2] = docObjectCreate();

//Hitboxes for charge attack, swing, explosion
hitboxes[2,0] = docObjectCreate();
hitboxes[2,1] = docObjectCreate();

//Attack speed can be modified seperately for each hit in the combo
attackSpeed[0] = 0.2;
attackSpeed[1] = 0.2;
attackSpeed[2] = 0.2;

//Charge attack
mistCost[0] = 30;
mistCost[1] = 20;
mistCost[2] = 20;

var _baseDamage;
_baseDamage = BASE_DAMAGE * 1.15;


/*      First slash         */
//Front
with (hitboxes[0,1]){
    damage = _baseDamage;
    
    stagger = 60;   knockback = 300;
    startFrame = 3; endFrame = 6;
    
    left = -1; top = -27;
    right = 42; bottom = 0;
    
    sprite = sprTriglavSlashFront;
}
//Up
with (hitboxCopy(hitboxes[0,1], hitboxes[0,0])){
    left = -1; top = -40;
    right = 34; bottom = -16;
    
    sprite = sprTriglavSlashUp;
}
//Down
with (hitboxCopy(hitboxes[0,1], hitboxes[0,2])){
    left = -1; top = -15;
    right = 38; bottom = 8;
    
    sprite = sprTriglavSlashDown;
}


/*      Second slash        */
//Front
with (hitboxes[1,1]){
    damage = _baseDamage * 1.15;
    
    stagger = 115;   knockback = 500;
    startFrame = 3; endFrame = 5;
    
    left = -24; top = -19;
    right = 47; bottom = -6;
    
    sprite = sprTriglavSlash2Front;
}
//Up
with (hitboxCopy(hitboxes[1,1], hitboxes[1,0])){
    left = 5; top = -41;
    right = 39; bottom = -20;
    
    sprite = sprTriglavSlash2Up;
}
//Down
with (hitboxCopy(hitboxes[1,1], hitboxes[1,2])){
    left = 5; top = -6;
    right = 45; bottom = 6;
    
    sprite = sprTriglavSlash2Down;
}


/*      Charge                  */
//Slam
with (hitboxes[2,0]){
    damage = _baseDamage * 1.4;
    
    stagger = 140;   knockback = 0;
    startFrame = 4; endFrame = 6;
    
    left = 0; top = -64;
    right = 32; bottom = 16;
}

//Explosion
with (hitboxes[2,1]){
    damage = _baseDamage * 1.5;
    
    stagger = 150;   knockback = 800;
    startFrame = 0; endFrame = 10;
    
    left = 32; top = -64;
    right = 64; bottom = 16;
}
