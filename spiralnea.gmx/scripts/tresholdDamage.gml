///diminishedDefence(real damage, real defence)
/*
    Actually SK's damage formula
*/

return argument0 * (1 - (1/2 + 0.19 * log10((argument1 - argument0)/15 + 1)));

/*
  damage * (1 - (1/2 + 0.19 * log10((defence - damage)/15 + 1)));
*/
