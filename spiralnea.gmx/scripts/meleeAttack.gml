///meleeAttack(objId hitboxObject, objId enemy, objId buffObject)
sndPlay3d(impactSound, false);

var _my;
_my = id;

with (argument1){
    getHit(
        argument0.damage * argument2.totalBuff[BUFF_DI],
        argument0.stagger * argument2.totalBuff[BUFF_DI],
        argument0.knockback,
        180 * (argument2.image_xscale < 1)
    );
}

if (hasStatus){
    with (argument1){
        statusApply(_my.status, _my.statusStrength);
    }
}
