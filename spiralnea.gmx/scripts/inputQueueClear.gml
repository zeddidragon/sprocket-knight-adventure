///inputQueueClear()
//Must be called from weapon.

ds_queue_clear(inputQueue);
inputQueueCount = 0;

var _i;
for (_i = 0; _i < 2; _i++){
    if (weapon[_i].willAttack){
        weapon[_i].willAttack = false;
        inputQueueReady = true;
    }
}
if (willJump || willShield){
    willJump = false; willShield = false;
    inputQueueReady = true;
}
