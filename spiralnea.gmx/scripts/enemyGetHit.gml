///takeDamage(real damage, int stagger, int knockback, real direction)

if (hp == 0){
    return false;
}

damageNumber(x, y, argument0);
hp = max(0, hp - argument0);

//Knockback
getKnockback(max(argument2 - weight, 0) * 0.01, argument3);

if (argument1 > 0){
    aiFaceTowards(oPlayer);
    sndPlay3d(onHitSound, false);
}

flashWhite();

//Stagger
if (currentStagger > staggerLimit || hp == 0){
    nextState = "hurt"; 
    currentStagger -= staggerLimit;
}
