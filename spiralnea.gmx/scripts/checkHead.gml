///checkHead()
//returns true if collision

var highest, groundY, startx, endx, starty, endy;

highest = room_height;
groundY = highest;

startx = max(0, (x-width) div TILESIZE);
endx = min(MapWidth-1, floor(x+width) div TILESIZE);
starty = max(0, (y-height) div TILESIZE);
endy = min(MapHeight-1, (y-4) div TILESIZE);

for (j = starty; j <= endy; j += 1){
  for (i = startx; i <= endx; i += 1){
    if (Map[i,j] == BLOCK)
      return true;
  }
}

return false;
