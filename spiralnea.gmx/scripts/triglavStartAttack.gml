///triglavStartAttack()
setToAttack();
isVisible  = false;

//Grunty noises on attack
if (choose(true, false)){
    sndPlay3d(choose(sndZeddyAttack, sndZeddyAttack2, sndZeddyAttack3), false);
}

ds_list_clear(hitList);

//Combo logic
combo += 1;

var _combo, _height;
_combo = combo - 1;
_height = heightModifier + 1;

hitbox = hitboxes[_combo, _height];
parent.sprite_index = hitbox.sprite;
parent.image_xscale = sign(parent.image_xscale + 2 * sideModifier);

hasStatus = consumeMist(parent, parent.hasMagic, mistCost[combo - 1]);

y  = parent.y;