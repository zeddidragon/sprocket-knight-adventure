///findFirstSortedWithCondition(objId object, script variable, script condition)
var _list = sortByVariable(argument0, argument1);
var _size = ds_list_size(_list);

var _value;

for (var _i = 0; _i < _size; _i++){
    _value = _list[|_i];
    if (_value != id && script_execute(argument2, _value)){
        ds_list_destroy(_list);
        return _value;
    }
}

ds_list_destroy(_list);
return noone;
