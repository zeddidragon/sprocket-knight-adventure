if (reversible && state || !state){
    state = !state;
    
    if(state){
        sprite_index = sprSwitchGreen;
    } else {
        sprite_index = sprSwitchRed;
    }
    
    image_speed = 1;
    sndPlay3d(sndHitSwitch, false);
    if (trigger[state] != ""){
        triggerTrigger(trigger[state]);
    }
}