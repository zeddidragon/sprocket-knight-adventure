if (countdown-- < 1){
    countdown = 0;
    fsmStartAnimationState(sprBeastbellRise, 0, sprite_get_number(sprBeastbellRise), animationSpeed, "idle");
} else {
    image_index = countdown * image_number / countdownMax;
}