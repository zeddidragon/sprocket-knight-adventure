///triggerSpawner(string name)
with (oSpawner){
    if (name == argument0){
       for (i = 0; i < ds_list_size(spawnLocationList); i++){
            spawnerSpawn(spawnLocationList[|i], spawnObjectList[|i]);
        }
    }
}
