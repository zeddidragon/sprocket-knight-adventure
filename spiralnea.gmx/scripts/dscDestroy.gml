///dscDestroy()
while (!ds_stack_empty(dsMapStack)){
    ds_map_destroy(ds_stack_pop(dsMapStack));
}
ds_stack_destroy(dsMapStack);

while (!ds_stack_empty(dsListStack)){
    ds_list_destroy(ds_stack_pop(dsListStack));
}
ds_stack_destroy(dsListStack);

while (!ds_stack_empty(dsQueueStack)){
    ds_queue_destroy(ds_stack_pop(dsQueueStack));
}
ds_stack_destroy(dsQueueStack);