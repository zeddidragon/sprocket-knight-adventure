///weaponQueueStore()
//Feed weapon with queued command.
//Must be called from parent of weapons.
with (parent){
    weaponQueueClear();
}

ds_queue_enqueue(parent.weaponQueue, id);

//Side modifier
ds_queue_enqueue(parent.weaponQueue, (
    isButtonHeld(parent.playerNumber, "right") -
    isButtonHeld(parent.playerNumber, "left")
) * !isButtonHeld(parent.playerNumber, "shield"));

//Height modifier
ds_queue_enqueue(parent.weaponQueue,
    isButtonHeld(parent.playerNumber, "down") -
    isButtonHeld(parent.playerNumber, "up")
);

parent.weaponQueueCount++;
