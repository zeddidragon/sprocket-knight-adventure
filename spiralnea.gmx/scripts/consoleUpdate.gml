///consoleUpdate(string subject, string message)
with (consoleGetMessageByLabel(argument0)){
    instance_destroy();
}

msg = instance_create(0, MessageY, oConsoleMessage);
msg.label = argument0;
msg.message = to_string(argument0) + ": " +  to_string(argument1);