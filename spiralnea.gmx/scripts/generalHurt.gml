if (aniTimer * totalBuff[BUFF_MSI] > staggerDuration){
    nextState = "fall";
    if (hp <= 0){
        nextState = "dead";
    }
}

if (touchGround){   
    image_speed = 0;
    image_index = image_number - 1;
    haveFriction();
} else {
    haveGravity();
    if (image_index + image_speed >= image_number - 1){
        image_speed = 0;
        image_index = image_number - 2;
    }
}
