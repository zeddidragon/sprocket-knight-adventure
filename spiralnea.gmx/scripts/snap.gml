///snap(real num)
//snaps back to divisible by 16
//argument0 number to snap

return (argument0 div TILESIZE) * TILESIZE;