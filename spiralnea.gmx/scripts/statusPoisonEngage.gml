///statusPoisonEngage()
statusSetDuration(16);

debuff = 0.5 * alarm[0] / (16 * 60);

var _debuff;
_debuff = debuff;

with (target){
    buffApply(BUFF_DEFENCE, "poison", 1 - _debuff * 0.5);
    buffApply(BUFF_DI, "poison", 1 - _debuff);
}

