willAttack = true;
sideModifier = (
    isButtonHeld(parent.playerNumber, "right") -
    isButtonHeld(parent.playerNumber, "left")
) * !isButtonHeld(parent.playerNumber, "shield");
heightModifier =
    isButtonHeld(parent.playerNumber, "down") -
    isButtonHeld(parent.playerNumber, "up")
;
