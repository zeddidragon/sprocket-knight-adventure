audio_stop_sound(chargeSound);
sndPlay3d(sndChargeDone, false);
chargeSound = sndPlay(sndChargeHum, true);

chargeDisplay.charged = true;
chargeDisplay.image_blend = make_color_rgb(200, 120, 0);
