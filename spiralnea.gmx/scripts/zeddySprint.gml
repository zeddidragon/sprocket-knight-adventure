if (isAtFrame(id, 2) || isAtFrame(id, 6)){
    sndPlay3d(stepSound, false);
}

if ((image_xscale > 0 && !isButtonHeld(playerNumber, "right")) ||
    (image_xscale < 0 && !isButtonHeld(playerNumber, "left"))){
    nextState = "idle";
    buffRemove(BUFF_MSI, "sprint");
}
