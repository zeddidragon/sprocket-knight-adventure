sndPlay3d(sndBeastbell, false);
    
var _my;
_my = id;

with (oEnemy){
    if (collision_circle(_my.x, _my.y - 20, _my.radius, id, false, false)){
        statusApply("stun", _my.stunStrength);
    }
}
    
sprite_index = sprBeastbellRing;
image_speed = ringAnimationSpeed;

instance_create(x, y - 20, oBellring);