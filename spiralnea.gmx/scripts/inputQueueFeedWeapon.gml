///inputQueueFeedWeapon()
//Feed weapon with queued command.
//Must be called from parent of weapons.
var _slotNumber, _sideModifier, _heightModifier;
_slotNumber     = ds_queue_dequeue(inputQueue);
_sideModifier   = ds_queue_dequeue(inputQueue);
_heightModifier = ds_queue_dequeue(inputQueue);

with (weapon[_slotNumber]){
    willAttack = true;
    sideModifier = _sideModifier;
    heightModifier = _heightModifier;
}