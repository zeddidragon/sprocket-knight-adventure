///sortByVariableobjId object, script variable/Script)
//returns a ds_list which needs to be destroyed externally
var _ret, _grid, _i, _my, _size;

_ret = ds_list_create();
_size = instance_number(argument0);
_grid = ds_grid_create(2, _size);
_i = 0;
_my = id;

with (argument0){
    _grid[# 0, _i] = id;
    _grid[# 1, _i] = script_execute(argument1, id);
    _i++;
}

ds_grid_sort(_grid, 1, true);

for (_i = 0; _i < _size; _i++){
    _ret[|_i] = _grid[# 0, _i];
}

ds_grid_destroy(_grid);

return _ret;
