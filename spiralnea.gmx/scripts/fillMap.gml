///fillMap()
clearMap();

with (oBlock){
  Map[x div TILESIZE, y div TILESIZE] = BLOCK;
  instance_destroy();
}

with (oWeakSlopeLeft){
  Map[x div TILESIZE, y div TILESIZE] = LHIGHSLOPE;
  Map[x div TILESIZE + 1, y div TILESIZE] = LLOWSLOPE;
  instance_destroy();
}

with (oWeakSlope){
  Map[x div TILESIZE, y div TILESIZE] = RLOWSLOPE;
  Map[x div TILESIZE + 1, y div TILESIZE] = RHIGHSLOPE;
  instance_destroy();
}

with (oSlopeLeft){
  Map[x div TILESIZE, y div TILESIZE] = LSLOPE;
  instance_destroy();
}

with (oSlope){
  Map[x div TILESIZE, y div TILESIZE] = RSLOPE;
  instance_destroy();
}
