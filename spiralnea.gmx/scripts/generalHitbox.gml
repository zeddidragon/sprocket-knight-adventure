///generalHitbox(objId hitboxObject, objId enemy, objId buffObject)

var _left, _top, _right, _bottom, _my, _target;
_left   = argument2.x   + argument0.left    * argument2.image_xscale
_right  = argument2.x   + argument0.right   * argument2.image_xscale
_top    = argument2.y   + argument0.top;
_bottom = argument2.y   + argument0.bottom;

/* DEBUG */
    hitboxDisplayAdd(_left, _top, _right, _bottom);
/* /DEBUG */

_my = id;

with (argument1){
    //If they collide with the sword's hitbox
    if (collision_rectangle(_left, _top, _right, _bottom, id, false, false)){
        if (ds_list_find_index(_my.hitList, id) < 0){ //Confirm we haven't struck this enemy already during this attack
            _target = id;
            
            with (_my){
                script_execute(meleeScript, argument0, _target, argument2);
            }
            
            ds_list_add(_my.hitList, id);   //Mark enemy as having been struck during this attack
        }
    }
}
