///pepperboxIdle()
if (willAttack){
    if (parent.state == "run" || parent.state == "idle" && parent.canAttack){
        parent.canAttack = false;
        if (charged){
            nextState = "charge attack";
        } else {
            nextState = "attack";
        }
    } else {
        willAttack = false;
    }
}
