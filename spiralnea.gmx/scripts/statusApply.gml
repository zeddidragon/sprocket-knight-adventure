///statusApply(string status, real strength)
var _total;

_total = clamp(
    argument1 - statusResistanceMap[?argument0],
    -6, 8
);

if (_total > -6){
    switch (argument0){
        case "stun":
            statusAddObject(argument0, statusStun, _total);
            break;
        case "freeze":
            statusAddObject(argument0, statusFreeze, _total);
            break;
        case "fire":
            statusAddObject(argument0, statusFire, _total);
            break;
        case "shock":
            break;
        case "poison":
            break;
    }
}
