///spawnShot(real relativeX, real relativeY, objId object, real angle)

var shot;

shot = instance_create(x + image_xscale * argument0, y + argument1, argument2);
shot.direction += argument3;
shot.hspeed *= image_xscale;
shot.image_angle = shot.direction;

return shot;