charge += parent.totalBuff[BUFF_CTR];

if (charge > 30 && !hasChargeDisplay){
    hasChargeDisplay = true;
    chargeDisplay = instance_create(x, y, oChargeDisplay);
    chargeDisplay.image_blend = make_color_rgb(80, 150, 120);
    chargeDisplay.pattern = chargePattern;
    chargeDisplay.charged = false;
    
    hasChargeSound = true;
    chargeSound = sndPlay(sndChargeUp, false);
} else if (hasChargeDisplay){
    chargeDisplay.x = x;
    chargeDisplay.y = y;
}

if (parent.state == "shield" || parent.state == "attack" || !parent.touchGround || !parent.canAttack){
    nextAiState = "charge pause";
} else if (charge > chargeCap){
    nextAiState = "charged";
}

if (!isButtonHeld(parent.playerNumber, button) || state != "idle"){
    nextAiState = "idle";
}
