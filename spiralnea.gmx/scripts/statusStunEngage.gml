///statusPoisonEngage()
statusSetDuration(6);

debuff = 0.5 * alarm[0] / (6 * 60);

var _debuff;
_debuff = 1 - debuff;

with (target){
    buffApply(BUFF_MSI, "stun", _debuff);
    buffApply(BUFF_ASI, "stun", _debuff);
    xspeed *= _debuff;
    yspeed *= _debuff;
}

