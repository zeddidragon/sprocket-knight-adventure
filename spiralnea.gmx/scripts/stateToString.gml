///stateToString(int state)
switch (argument0){
    case-1: return "NONE";
    case 0: return "IDLE";
    case 1: return "RUNNING";
    case 2: return "JUMPING";
    case 3: return "FALLING";
    case 4: return "ATTACK";
    case 5: return "CHARGE";
    case 6: return "HURT";
    case 7: return "DEAD";
    case 8: return "USE";
    case 9: return "ANIMATE";
    default: return string(argument0);
}
