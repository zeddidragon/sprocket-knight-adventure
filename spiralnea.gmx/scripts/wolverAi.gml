if (ButtonPressed[0, LEFT]){
    nextState = "run";
    image_xscale = -1;
} else if (ButtonPressed[0, RIGHT]){
    nextState = "run";
    image_xscale = 1;
} else if (state != "idle" && !(ButtonHeld[0, LEFT] || ButtonHeld[0, RIGHT])){
    nextState = "idle";
}

if (ButtonPressed[0, J]){
    nextState = "telegraph-attack";
}
