///plotCourse(real xDest, real yDest, frames time)

//argument0 x-destination
//argument1 y-destination
//argument2 time to get there (frames)

speed = point_distance(x, y, argument0, argument1) / argument2;
direction = point_direction(x, y, argument0, argument1);
alarm[0] = argument2;