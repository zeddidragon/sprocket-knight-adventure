///iniControls()

globalvar ControllerMap, GamepadMap, PressedMap, HeldMap, ReleasedMap;

//Player 1

ControllerMap[0] = ds_map_create();
GamepadMap[0] = ds_map_create();

HeldMap[0] = ds_map_create();
PressedMap[0] = ds_map_create();
ReleasedMap[0] = ds_map_create();

map = ControllerMap[0];
map[?"up"] = ord('W');
map[?"down"] = ord('S');
map[?"left"] = ord('A');
map[?"right"] = ord('D');

map[?"jump"] = ord('I');
map[?"attack"] = ord('J');
map[?"shield"] = ord('K');
map[?"sidearm"] = ord('O');

map[?"magic"] = vk_shift;
map[?"sprite"] = vk_space;
map[?"sprint"] = ord('F');

map[?"pause"] = ord('P');
map[?"select"] = ord('R');

map = GamepadMap[0];
map[?"up"] = gp_padu;
map[?"down"] = gp_padd;
map[?"left"] = gp_padl;
map[?"right"] =gp_padr;

map[?"jump"] = gp_face2;
map[?"attack"] = gp_face4;
map[?"shield"] = gp_shoulderlb;
map[?"sidearm"] = gp_face3;

map[?"magic"] = gp_shoulderrb;
map[?"sprite"] = gp_shoulderr;
map[?"sprint"] = gp_face1;

map[?"pause"] = gp_start;
map[?"select"] = gp_select;

//Player 2

ControllerMap[1] = ds_map_create();
GamepadMap[1] = ds_map_create();

HeldMap[1] = ds_map_create();
PressedMap[1] = ds_map_create();
ReleasedMap[1] = ds_map_create();

map = GamepadMap[1];
map[?"up"] = gp_padu;
map[?"down"] = gp_padd;
map[?"left"] = gp_padl;
map[?"right"] =gp_padr;

map[?"jump"] = gp_face2;
map[?"attack"] = gp_face4;
map[?"shield"] = gp_shoulderlb;
map[?"sidearm"] = gp_face3;

map[?"magic"] = gp_shoulderrb;
map[?"sprite"] = gp_shoulderr;
map[?"sprint"] = gp_face1;

map[?"pause"] = gp_start;
map[?"select"] = gp_select;
