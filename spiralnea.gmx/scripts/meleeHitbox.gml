///meleeHitbox(objId hitboxObject, objId enemy)
//Handles collision checking for melee attacks from non-seperate objects

//First, confirm we're at the animaton frames where the sword should strike
if (image_index > argument0.startFrame && image_index < argument0.endFrame){
    generalHitbox(argument0, argument1, id);
}