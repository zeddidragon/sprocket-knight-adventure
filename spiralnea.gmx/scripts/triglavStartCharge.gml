///triglavSTartCharge()
with (parent){
    playerMoveH(true, true);
}
setToAttack();

charged = false;
isVisible = false;

ds_list_clear(hitList);

if (choose(true, false)){
  sndPlay3d(sndZeddyChargeTriglav, false);
}

combo = 1;
hasStatus = consumeMist(parent, parent.hasMagic, mistCost[2]);

parent.sprite_index = sprTriglavCharge;

y = parent.y;