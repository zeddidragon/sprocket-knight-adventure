///hitboxCopy(objId source, objId destination)
argument1.damage        = argument0.damage;
argument1.stagger       = argument0.stagger;
argument1.knockback     = argument0.knockback;

argument1.startFrame    = argument0.startFrame;
argument1.endFrame      = argument0.endFrame;

argument1.left          = argument0.left;
argument1.top           = argument0.top;
argument1.right         = argument0.right;
argument1.bottom        = argument0.bottom;

return argument1;