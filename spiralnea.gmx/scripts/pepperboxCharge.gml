///pepperboxCharge()
advanceFrame(parent, 0, parent);
    
if (isAtFrame(parent, 6) || isAtFrame(parent, 6)){

    shot = spawnShot(shotX, shotY, oPepperShot, -10 + (floor(parent.image_index - 6)) * 5 + 10 * (combo mod 2) - aimHeight * 22.5);
    shot.hasFire = consumeMist(parent, parent.hasMagic, 10);
    shot.knockback *= 1.5;
    shot.stagger *= 2;
    
    
    spawnAnimation(0, 0, muzzleSprite, 1, image_xscale, 0);
    part_particles_create(PartSys, x + casingX, y + casingY, PartPepperCasing, 1);
    ammo -= 1;
    sndPlay3d(sndGunShot, false);
}

if (isAtFrame(parent, parent.image_number)){
    combo += 1;
    if (ammo <= 0){
        nextState = "reload";
    } else{
        parent.image_index = 3;
    }
}
