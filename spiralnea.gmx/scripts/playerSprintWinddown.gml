if (xspeed != 0){
    xspeed = (abs(xspeed) - totalAcceleration * 0.25) * sign(xspeed);
    if (abs(xspeed) < 0.2){
        xspeed = 0;
        aniTimer = 0;
    }
} else if (aniTimer * totalBuff[BUFF_MSI] > 10){
    nextState = "idle";
}

if (!touchGround){
    nextState = "fall";
}
