/// adjustAppSurface()
winWidth = browser_width;
winHeight = browser_height;

Scaling = min(winWidth / width, winHeight / height);
offsetX = floor((winWidth - width * Scaling) * 0.5);
offsetY = floor((winHeight - height * Scaling) * 0.5);
