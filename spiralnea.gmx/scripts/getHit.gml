///getHit(damage, stagger, knockback, direction)

var _damage, _defence;

_defence = defence * totalBuff[BUFF_DEFENCE];

if (_defence > argument0){
  _damage = ceil(tresholdDamage(argument0, _defence));
} else {
  _damage = ceil(argument0 - _defence * 0.5);
}

currentStagger += argument1;

script_execute(getHitScript, _damage, argument1, argument2, argument3);

statusGetHit();
