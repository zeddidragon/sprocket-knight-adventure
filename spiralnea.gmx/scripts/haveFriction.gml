///haveFriction()

if (xspeed != 0){
  xspeed -= sign(xspeed) * totalAcceleration;
  if (abs(xspeed) < totalAcceleration * 2)
     xspeed = 0;
}