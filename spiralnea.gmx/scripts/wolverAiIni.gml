/*
    state : passive
    substate : idling
        wait for x seconds, then walk to random spot
        if see player, enter aggressive state
    substate : patroling
        move to random spot, then back to idling
        if see player, enter aggressive state
        
    state : aggressive
    substate : alert
        wait for x seconds, shorter than idling
        alert Decision
    substate : closing
        move to random spot towards player
        alert Decision
    substate : attacking
        attack
        alert
        
    subroutine: alert decision
        if player is close, attack
        if player is far, move closer

*/

alert = false;

aiTimer = 0;
aiEndTime = 0;

idleMinTime = 1 * 60;
idleMaxTime = 3 * 60;

patrolMinTime = 0.5 * 60;
patrolMaxTime = 1.5 * 60;

aiDetectRange = 192;
aiVerticalDetect = 64;

attackRange = 72;

fsmAddAi("idle", wolverAiStartIdle, wolverAiIdle);
fsmAddAi("patrol", wolverAiStartPatrol, wolverAiPatrol);
fsmAddAi("alert", wolverAiStartAlert, fsmAiIdleState);
fsmAddAi("alert decision", dummyScript, wolverAiAlertDecision);
fsmAddAi("hunt", wolverAiStartHunt, fsmAiIdleState);

nextAiState = "idle";
