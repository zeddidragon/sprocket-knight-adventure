///docDestroy
//Must be run before dscDestroy()
for (i = 0; i < ds_list_size(docList); i++){
    with (docList[|i]){
        instance_destroy();
    }
}
