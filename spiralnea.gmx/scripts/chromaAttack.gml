tongueLength += tongueSpeed * totalBuff[BUFF_ASI];

var _x;
_x = x;
x += tongueLength * image_xscale;

meleeHitbox(hitbox, oPlayer);

x = _x;

if (tongueLength >= maxTongueLength){
    tongueLength = maxTongueLength;
    ds_list_clear(hitList);
    fsmStartIdleState(retractDelay / totalBuff[BUFF_ASI], "retract");
}
