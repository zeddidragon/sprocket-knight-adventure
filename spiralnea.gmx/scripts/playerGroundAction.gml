///playerGroundAction()
if (!touchGround){
  nextState = "fall";
} else if (willJump){
    script_execute(jumpScript);
} else if (willShield){
    nextState = "shield";
    inputQueueConsume();
    willShield = false;
} else if (wasButtonPressed(playerNumber, "sprint") && state != "sprint"){
    script_execute(sprintScript);
}

if (isButtonHeld(playerNumber, "shield")){
    inputQueueStore(id, "shield");
}
