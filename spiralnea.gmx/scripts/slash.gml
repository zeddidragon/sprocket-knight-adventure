var master;
master = id;

with (instance_create(x+image_xscale*8, y, oSlash)){
  image_xscale = master.image_xscale;
  sprite_index = argument0;
  damage = argument1;
  push = argument2;
  lift = argument3;
}
