///triglavAttack()
advanceFrame(parent, combo - 1, parent);
  
if (isAtFrame(parent, 2)){
  sndPlay3d(sndSlash2, false);
}

swordHitbox(hitbox, oEnemy);

if (isAtFrame(parent, parent.image_number)){
  if (willAttack && combo < 2){
    inputQueueConsumeWeapon();
    nextState = "attack";
  } else {
    nextState = "idle";
    if (!isButtonHeld(parent.playerNumber, button) || combo > 1){
      combo = 0;
      comboTimer = 40;
    } else {
      comboTimer = 90;
    }
  }
}