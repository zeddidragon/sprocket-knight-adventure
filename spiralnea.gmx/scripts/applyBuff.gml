///applyBuff(const buffType, string name, real modifier)
if (ds_map_exists(buffMap[argument0], argument1)){
    ds_map_delete(buffMap[argument0], argument1);
}

ds_map_add(buffMap[argument0], argument1, argument2);

calculateTotalBuff(argument0);
