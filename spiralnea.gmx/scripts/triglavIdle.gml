///triglavIdle()
if (willAttack && parent.canAttack && parent.touchGround){
    if (comboTimer <= 0){
        inputQueueConsumeWeapon();
        parent.canAttack = false;
        if (charged){
            nextState = "charge attack";
        } else {
            nextState = "attack";
        }
    }
}