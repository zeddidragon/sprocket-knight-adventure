///puppyPitch()
pitch += movementModifier * pitchSpeed;

if (pitch < 0 || pitch > 1){
    pitch = max(0, sign(pitch));
    nextState = "idle";
}

//Converts pitch to image_index
puppyConvertPitch();
