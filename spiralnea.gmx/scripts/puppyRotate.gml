///puppyRotate()
facing += movementModifier * turnSpeed;

if (facing < -1 || facing > 1){
    facing = sign(facing);
    nextState = "idle";
}

//Converts facing to image_index
image_index = image_number - abs(facing) * image_number;
image_xscale = sign(facing);
if (abs(facing) < 0.1){
    image_xscale = 1;
    image_index = image_number-1;
}
