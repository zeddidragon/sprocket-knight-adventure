///checkFeet(real topY, real bottomY)
//returns y-value of highest piece of ground touching feet

var highest, groundY, startx, endx, starty, endy;

highest = room_height;
groundY = highest;
startx = max(0, (x-width) div TILESIZE);
endx = min(MapWidth-1, floor(x+width) div TILESIZE);
starty = max(0, (argument0) div TILESIZE);
endy = min(MapHeight-1, (argument1) div TILESIZE);

for (j = starty; j <= endy; j += 1){
  for (i = startx; i <= endx; i += 1){
    if Map[i,j] == CLEAR
      continue;
    baseX = i * TILESIZE;
    baseY = j * TILESIZE;
    switch (Map[i, j]){
      case BLOCK:
        groundY = baseY;
        break;     
      case RSLOPE:
        groundY = baseY+TILESIZE-(x+width-baseX);
        break;
      case LSLOPE:
        groundY = baseY+(x-width-baseX);
        break;
      case RLOWSLOPE:
        groundY = baseY+TILESIZE-(x+width-baseX)*0.5;
        groundY = max(groundY, baseY+HALFTILE);
        break;
      case RHIGHSLOPE:
        groundY = baseY+HALFTILE-(x+width-baseX)*0.5;
        groundY = min(groundY, baseY+HALFTILE);
        break;
      case LLOWSLOPE:
        groundY = baseY+HALFTILE+(x-width-baseX)*0.5;
        groundY = max(groundY, baseY+HALFTILE);
        break;
      case LHIGHSLOPE:
        groundY = baseY+(x-width-baseX)*0.5;
        groundY = min(groundY, baseY+HALFTILE);
        break;
    }
    highest = min(highest, clamp(groundY, baseY, baseY+TILESIZE));
    
  }
  if (highest < room_height)
    break;
}

return highest;