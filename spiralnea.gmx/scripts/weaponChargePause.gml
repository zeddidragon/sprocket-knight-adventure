if (hasChargeDisplay){
    chargeDisplay.x = x;
    chargeDisplay.y = y;
}

if (parent.state != "shield" && parent.state != "attack" && parent.touchGround && parent.canAttack){
    nextAiState = "charge";
}

if (!isButtonHeld(parent.playerNumber, button) || state != "idle"){
    nextAiState = "idle";
}
