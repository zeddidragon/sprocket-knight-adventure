nextState = "run";

image_xscale = 1 - 2 * ((random(100) > 50 || isBlockAt(x + width + TILESIZE, y - 8)) && !isBlockAt(x - width - TILESIZE, y - 8));

aiTimer = 0;
aiEndTime = irandom_range(patrolMinTime, patrolMaxTime);
