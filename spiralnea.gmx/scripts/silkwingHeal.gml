///meleeHeal(int hitboxIndex, objId enemy)
sndPlay3d(sndHeal);

var _heal, _poison;
_poison = statusFind("poison");
_heal = damage[argument0]

if (_poison >= 0){
    with (argument0){
        getHit();
    }
} else {
    //TODO
    statusInflictDamage(argument1, _heal * _poison.strength);
}
