///puppyAi()

if (instance_exists(oPlayer)){
    aiCooldown -= 1;
    
    if (sign(oPlayer.x - x) != facing){
        nextState = "turn";
        aiCooldown -= 20;
    } else if (pitch == (oPlayer.y > y - 64)){
        nextState = "pitch";
        aiCooldown -= 5;
    } else if (aiCooldown < 0){
        aiCooldown = fireCooldown;
        puppyEngageAttack();
    }
}
