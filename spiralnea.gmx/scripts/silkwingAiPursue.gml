if (instance_exists(target)){
    if (abs(x - target.x) > 64){
        silkwingHover();
        targetX = (target.x + 3 * x) * 0.25;
    } else {
        targetY = target.y - 20;
        targetX = (target.x + x) * 0.5;
    }
    nextState = "run";
    
    if (target.hp >= target.maxHp){
        nextAiState = "idle";
    }
} else {
    nextAiState = "idle";
}