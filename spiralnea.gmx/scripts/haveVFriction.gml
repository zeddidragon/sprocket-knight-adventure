///haveVFriction()

if (yspeed != 0){
  yspeed -= totalAcceleration*sign(yspeed);
  if (abs(yspeed) < totalAcceleration * 2)
     yspeed = 0;
}