///weaponQueueFeed()
//Feed weapon with queued command.
//Must be called from parent of weapons.
var _queue;
_queue = weaponQueue;

with (ds_queue_dequeue(_queue)){
    willAttack = true;
    sideModifier = ds_queue_dequeue(_queue);
    heightModifier = ds_queue_dequeue(_queue);
}

weaponQueueReady = false;
weaponQueueCount--;
