nextState = "run";

if (aiFaceTowards(oPlayer)){    
    var distance, time;
    distance = max(0, abs(oPlayer.x - x) - 32);
    time = distance / (RUN_SPEED * totalBuff[BUFF_MSI]);
    fsmAiStartIdleState(random_range(time * 0.5, time), "alert decision");
}
