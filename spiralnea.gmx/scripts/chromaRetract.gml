tongueLength -= tongueSpeed * totalBuff[BUFF_ASI];

var _x;
_x = x;
x += tongueLength * image_xscale;

image_xscale = -image_xscale; //Make attack hit backwards
meleeHitbox(hitbox, oPlayer);
image_xscale = -image_xscale;

x = _x;

if (tongueLength <= 0){
    nextState = "idle";
    sndPlay3d(sndChromaGulp, false);
}
