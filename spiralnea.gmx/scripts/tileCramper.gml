///tileCramper(string fileName, int tileSize)
var fname, tilespace, image, surf, width, height;
fname = argument0;
tilespace = argument1;

image = background_add(fname, true, false);
width = background_get_width(image);
height = background_get_height(image);

surf = surface_create(width, height);
surface_set_target(surf);
draw_clear_alpha(c_black, 0);
for (j = 0; j * (tilespace+1) < height; j+=1){
  for (i = 0; i * (tilespace+1) < width; i+=1){
    draw_background_part(image, (tilespace+1)*i, (tilespace+1)*j, tilespace, tilespace, tilespace*i, tilespace*j);
  }
}

surface_save(surf, fname+"-Compressed.png");
surface_reset_target();
surface_free(surf);

