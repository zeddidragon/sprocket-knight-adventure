if (wasButtonPressed(parent.playerNumber, button)){
    inputQueueStoreWeapon();
}

if (isButtonHeld(parent.playerNumber, button) && state == "idle"){
    nextAiState = "charge";
}

if (hasChargeDisplay){
    hasChargeDisplay = false;
    with (chargeDisplay){
        instance_destroy();
    }
}
    
if (hasChargeSound){
    hasChargeSound = false;
    audio_stop_sound(chargeSound);
} 
