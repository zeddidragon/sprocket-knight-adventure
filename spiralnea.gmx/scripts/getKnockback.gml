///knockback(real knockback, real direction)
if (!canKnockback){
    return false;
}

knockbackXSpeed += lengthdir_x(argument0, argument1);
knockbackYSpeed += lengthdir_y(argument0, argument1);
