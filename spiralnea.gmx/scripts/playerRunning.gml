playerMoveH(true, true);
if (isAtFrame(id, 2) || isAtFrame(id, 6)){
    sndPlay3d(stepSound, false);
}
if (
    xspeed == 0 && !isButtonHeld(playerNumber, "left") &&
    !isButtonHeld(playerNumber, "right")
){
  nextState = "idle";
}
image_speed = 0.1 + 0.1 * abs(xspeed); //Scroll frame
playerGroundAction();
actionKey();
