///statusGetHit(real maxTime)

var _key, _obj;
_key = ds_map_find_first(statusObjectMap);

repeat(ds_map_size(statusObjectMap)){
    _obj = ds_map_find_value(statusObjectMap, _key);
    
    if (_obj.hasHitScript){
        with(_obj){
            script_execute(hitScript);
        }
    }
}
