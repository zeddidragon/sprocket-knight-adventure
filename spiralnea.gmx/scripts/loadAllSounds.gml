///Returns a ds_map with sounds from all the sound resources in a folder
///argument0 is folder to look in
///argument1 is file extension
///argument2 is 3d true/false

var i, ret;

ret = ds_map_create();

var _file, _sound;
_file = file_find_first(argument0 + "/*." + argument1, 0);

while (_file != ""){
    _sound = loadSoundEffect(argument0 + "/" +_file, argument2);
    ds_map_add(ret, string_replace(_file, "." + argument1, ""), _sound);
    _file = file_find_next();
}
file_find_close();

return ret;
