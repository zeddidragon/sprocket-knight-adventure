image_speed = 0.2 * totalBuff[BUFF_ASI];
sprite_index = runSprite;

xspeed = lengthdir_x(RUN_SPEED, point_direction(x, y, targetX, targetY)) * totalBuff[BUFF_MSI];
yspeed = lengthdir_y(RUN_SPEED, point_direction(x, y, targetX, targetY)) * totalBuff[BUFF_MSI];

stopTimer = point_distance(x, y, targetX, targetY) / (RUN_SPEED * totalBuff[BUFF_MSI]);

image_xscale = sign(image_xscale + 2 * sign(xspeed));

